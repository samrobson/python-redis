# Python Redis

A basic implementation of a Redis-like database server, in Python.

My aims with this project were twofold:
- Learn about Redis
- Practice Python

The `Server` implements the `Redis` protocol, and the project includes
a `Client` to facilitate manual testing. The operations permitted are:
```
'GET'
'SET'
'DELETE'
'FLUSH'
'MGET'
'MSET'
```

Write operations will be saved to a local log, which can then be used to restore the state of the store.