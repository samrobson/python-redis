from gevent.pool import Pool
from gevent.server import StreamServer

from store import Store as KvStore
from protocol_handler import ProtocolHandler
from log_handler import LogHandler
from exceptions import CommandError, Disconnect, Error


class Server(object):
    def __init__(self, host='127.0.0.1', port=31337, max_clients=64):
        self._pool = Pool(max_clients)
        self._server = StreamServer(
            (host, port),
            self.connection_handler,
            spawn=self._pool)

        self._kv = KvStore()
        self._log = LogHandler(self._kv)
        self._protocol = ProtocolHandler()
        self._commands = self._kv.get_commands()

    def connection_handler(self, conn, address):
        # Convert "conn" (a socket object) into a file-like object.
        socket_file = conn.makefile('rw')

        # Process client requests until client disconnects.
        while True:
            try:
                data = self._protocol.handle_request(socket_file)
            except Disconnect:
                break

            try:
                resp = self.get_response(data)
            except CommandError as exc:
                resp = Error(exc.args[0])

            self._protocol.write_response(socket_file, resp)

    def get_response(self, data):
        if not isinstance(data, list):
            try:
                data = data.split()
            except:
                raise CommandError('Request must be list or simple string.')

        if not data:
            raise CommandError('Missing command')

        command = data[0].upper()
        if command not in self._commands:
            if command == 'REPLAY':
                return self._log.replay()
            else:
                raise CommandError('Unrecognized command: %s' % command)

        return self._kv.handle_operation(command, data)

    def run(self):
        self._server.serve_forever()


if __name__ == '__main__':
    Server().run()
