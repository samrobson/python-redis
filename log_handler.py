import pickle

logfile = 'history.log'

class LogHandler:
    def __init__(self, kv_store):
        self.logfile = logfile
        # only need to append write operations to the log
        self.commands = ['SET', 'DELETE', 'FLUSH', 'MSET']
        self._kv = kv_store

    def append(self, data):
        command = data[0].upper()
        if command in self.commands:
            with open(self.logfile, 'ab') as log:
                pickle.dump(data, log)

    def replay(self):
        with open(self.logfile, 'rb') as log:
            counter = 0
            while True:
                try:
                    counter = counter + 1
                    data = pickle.load(log)
                    command = data[0].upper()
                    self._kv.handle_operation(command, data)
                except EOFError:
                    return counter
